// ==UserScript===================================================
// @name         Togglemonkey
// @namespace    https://metalfenr.is
// @author       Joshua J. Riojas
// @version      1.0
// @description  View current page at different test/prod/dev site
// @match        http://development.site*
// @match        http://production.site*
// ==/UserScript==================================================

"use strict";

var prod = "http://production.site"
  , dev = "http://development.site";

function servEval(e) {
    var val = (e === prod) ? dev : prod;
    return val;
}

function servSwitch() {
    var url = window.location.protocol + "//" + window.location.hostname
      , page = window.location.pathname
      , newServ = servEval(url) + page;
    window.location.href = newServ;
}

function keyEval(e) {
    var keycode = e.keyCode || e.charCode;
    if (keycode === 192)
        servSwitch();
}

document.body.addEventListener("keydown", keyEval, false);

